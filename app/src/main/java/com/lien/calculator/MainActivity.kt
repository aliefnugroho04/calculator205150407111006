package com.lien.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*
import org.mariuszgromada.math.mxparser.Expression
import java.text.DecimalFormat
import android.text.Editable
import androidx.appcompat.app.AppCompatDelegate

class MainActivity : AppCompatActivity() {

    private lateinit var input: TextView
    private lateinit var output: TextView
    private lateinit var btnClear: Button
    private lateinit var btn1: Button
    private lateinit var btn2: Button
    private lateinit var btn3: Button
    private lateinit var btn4: Button
    private lateinit var btn5: Button
    private lateinit var btn6: Button
    private lateinit var btn7: Button
    private lateinit var btn8: Button
    private lateinit var btn9: Button
    private lateinit var btn0: Button
    private lateinit var btnAddition: Button
    private lateinit var btnSubtraction: Button
    private lateinit var btnMultiply: Button
    private lateinit var btnDivision: Button
    private lateinit var btnEquals: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        input=findViewById(R.id.tv_input)
        output=findViewById(R.id.tv_result)
        btnClear=findViewById(R.id.btn_clear)
        btn0=findViewById(R.id.btn0)
        btn1=findViewById(R.id.btn1)
        btn2=findViewById(R.id.btn2)
        btn3=findViewById(R.id.btn3)
        btn4=findViewById(R.id.btn4)
        btn5=findViewById(R.id.btn5)
        btn6=findViewById(R.id.btn6)
        btn7=findViewById(R.id.btn7)
        btn8=findViewById(R.id.btn8)
        btn9=findViewById(R.id.btn9)
        btnAddition=findViewById(R.id.btn_addition)
        btnSubtraction=findViewById(R.id.btn_subtraction)
        btnMultiply=findViewById(R.id.btn_multiply)
        btnDivision=findViewById(R.id.btn_division)
        btnEquals=findViewById(R.id.btn_equals)

        btnClear.setOnClickListener {
            input.text=""
            output.text=""
        }

        btn0.setOnClickListener {
            input.text=addToInputText("0")
        }
        btn1.setOnClickListener {
            input.text=addToInputText("1")
        }
        btn2.setOnClickListener {
            input.text=addToInputText("2")
        }
        btn3.setOnClickListener {
            input.text=addToInputText("3")
        }
        btn4.setOnClickListener {
            input.text=addToInputText("4")
        }
        btn5.setOnClickListener {
            input.text=addToInputText("5")
        }
        btn6.setOnClickListener {
            input.text=addToInputText("6")
        }
        btn7.setOnClickListener {
            input.text=addToInputText("7")
        }
        btn8.setOnClickListener {
            input.text=addToInputText("8")
        }
        btn9.setOnClickListener {
            input.text=addToInputText("9")
        }

        btnAddition.setOnClickListener {
            input.text=addToInputText("+")
        }
        btnSubtraction.setOnClickListener {
            input.text=addToInputText("-")
        }
        btnMultiply.setOnClickListener {
            input.text=addToInputText("*")
        }
        btnDivision.setOnClickListener {
            input.text=addToInputText("/")
        }
        btnEquals.setOnClickListener {
            showResult()
        }
    }

    private fun addToInputText(buttonValue: String): String {
        return input.text.toString() + "" + buttonValue
    }

    private fun getInputExpression(): String {
        return input.text.replace(Regex(":"), "/")
    }

    private fun showResult() {
        try {
            val expression = getInputExpression()
            val result = Expression(expression).calculate()
            if (result.isNaN()) {
                // Show Error Message
                output.text = ""
                output.setTextColor(ContextCompat.getColor(this, R.color.red))
            } else {
                // Show Result
                output.text = DecimalFormat("0.######").format(result).toString()
                output.setTextColor(ContextCompat.getColor(this, R.color.green))
            }
        } catch (e: Exception) {
            // Show Error Message
            output.text = ""
            output.setTextColor(ContextCompat.getColor(this, R.color.red))
        }
    }
}
